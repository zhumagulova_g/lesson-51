import React from "react";
import './App.css';
import Lottery from "./Lottery/Lottery"

class App extends React.Component {

    state = {
        arrayNum: [null, null, null, null, null]
    };

    newNumbers = (arrayNum) => {
        for (let i = 0; i < 5; i++) {
            const number = 4 + Math.floor(Math.random() * 32 + 1);
            if (arrayNum.indexOf(number) !== -1) {
                i--;
                continue;
            }
            arrayNum[i] = number;
        }
        return arrayNum;
    };

    compareNumeric = (a, b) => {
        if (a > b) return 1;
        if (a === b) return 0;
        if (a < b) return -1;
    };

    changeNumbers = () => {
        const arrayNum = [...this.state.arrayNum];
        this.newNumbers(arrayNum);

        arrayNum.sort(this.compareNumeric);

        this.setState( {arrayNum});
    };

    render () {
        return (
            <div className="App">
                <Lottery number1={this.state.arrayNum[0]}
                         number2={this.state.arrayNum[1]}
                         number3={this.state.arrayNum[2]}
                         number4={this.state.arrayNum[3]}
                         number5={this.state.arrayNum[4]}/>

                <button className="btn" onClick={this.changeNumbers}>New numbers</button>
            </div>
        );
    }
}

export default App;
